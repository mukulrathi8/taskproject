<?php

namespace App\Http\Controllers;

use App\Models\UserConnectedOauth;
use GuzzleHttp\Client as Client;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\OAuth2\Client\Provider\AbstractProvider;

class BaseController extends Controller
{

    public function request($bodyParams, $Uri, $method, $headers): array
    {

        //dd($bodyParams);

        try {
            $client = new Client();
            $body = ($method == 'POST') ? ['headers' => $headers, 'form_params' => $bodyParams] : ['headers' => $headers, 'query' => $bodyParams];

            $response = $client->request($method, $Uri, $body);
            if (!empty($response)) {
                if (!empty($response->getBody())) {
                    return $this->parseResponse($response->getBody());
                }
            } else {
                return ['status' => false];
            }
        } catch (\Exception $exception) {
            return ['status' => false, 'data' => $exception->getMessage()];
        }
    }

    public function parseResponse($response): array
    {
        $data = json_decode($response, true);
        return ['status' => true, 'data' => $data];
    }


    public function response($data, $code,$message=null): JsonResponse
    {

        return response()->json([
            'flag' => !($code != 200 && $code != 201), 
            'code' => $code,
            'message'=>$message,
            'data' => $data,
        ]);
    }





}