<?php

namespace App\Http\Controllers\api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Http\Controllers\{BaseController};
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    public function createAccount(Request $request): JsonResponse
    {

        try {
            $validate = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users,email',
                'password' =>  'required|string|min:5'
            ]);
            if ($validate->fails()) {
                return $this->response(['error' => $validate->errors()], 422);
            } else {
                $user = User::create([
                    'name' => $request->post('name'),
                    'password' => bcrypt($request->post('password')),
                    'email' => $request->post('email'),
                    'row_guid' => Str::orderedUuid()
                    // 'token'=> $user->createToken('tokens')->plainTextToken
                ]);
                $token = $user->createToken('tokens')->plainTextToken;
                $user->token = $token;

            }
            return $this->response(['user' => $user], 200, 'Account Created Successfully');
        } catch (\Exception $e) {
            return $this->response([
                'message' => $e->getCode() .' '. $e->getMessage(),
            ], $e->getCode(), );
        }
    }


    public function signIn(Request $request): JsonResponse
    {

        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email|',
            'password' => 'required|string|min:5',
        ]);

        if ($validate->fails()) {
            return $this->response(['error' => $validate->errors()], 422);
        } else {
            if (!Auth::attempt($request->only('email', 'password'))) {
                return $this->response(['error' => ['password' => ['Credentials not match']]], 422);
            } else {
                $token = auth()->user()->createToken('tokens')->plainTextToken;
                auth()->user()->token = $token;
                return $this->response(['user' => auth()->user()], 200,'Login Successfully');
            }
        }
    }

    public function signOut()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Tokens Revoked'
        ];
    }
}
