<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\User;

class ProjectController extends BaseController
{
    public function createProject( Request $request):JsonResponse{

      try {
            $validate = Validator::make($request->all(), [
                'name' => 'required|string|min:10',
                'description' => 'required',
            
            ]);
            if ($validate->fails()) {
                return $this->response(['error' => $validate->errors()], 422);
            } else {
                $project = Project::create([
                    'name' => $request->post('name'),
                    'created_by'=>auth()->user()->id,
                    'description' => $request->post('description'),
                    'row_guid' => Str::orderedUuid()
                ]);

                $project->projectUsers()->create([
                "user_id" => auth()->user()->id,
                'row_guid'=>Str::orderedUuid()]);   
                return $this->response([], 200, 'Project Created Successfully');
            }
        } catch (\Exception $e) {
            return $this->response([
                'message' => $e->getCode() .' '. $e->getMessage(),
            ], $e->getCode(), );
        }
    }


    public function listProjects ( $projectGuid =null): JsonResponse {
       
        $projectQuery = auth()->user()->listProject();

        if (!is_null($projectGuid)) {
            $projectQuery->where('row_guid', $projectGuid);
        }
        
        $projects = $projectQuery->orderBy('id','desc')->get();
        
        return $this->response($projects, 200, 'User Created Project List');
    }

    public function listProjectTask ($projectGuid): JsonResponse
    {
        $project = Project::where(['row_guid'=>$projectGuid])->with('projectTasks')->get();
        return $this->response($project, 200, 'Task List');
    }

    public function update(Request $request, $projectGuid): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required|string|min:10',
            'description' => 'required',
        
        ]);
        if ($validate->fails()) {
            return $this->response(['error' => $validate->errors()], 422);
        }else{
            Project::where(['row_guid'=>$projectGuid])->update($request->all());
            return $this->response([], 201,'Project Updated');
        }
   
    }
}
