<?php

namespace App\Http\Controllers\api;


use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectTask;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\User;


class ProjectTaskController extends BaseController
{
    public function addTask(Request $request): JsonResponse
    {

        try {
            $validate = Validator::make($request->all(), [
                'name' => 'required|string|min:10',
                'description' => 'required',
                'priority' => 'required',
                'due_date' => 'required',
                'status' => 'required'

            ]);
            if ($validate->fails()) {
                return $this->response(['error' => $validate->errors()], 422);
            } else {
                ProjectTask::create([
                    'name' => $request->post('name'),
                    'description' => $request->post('description'),
                    'row_guid' => Str::orderedUuid(),
                    'status' => $request->post('status'),
                    'due_date' => $request->post('due_date'),
                    'priority' => $request->post('priority'),
                    'project_id' => $request->post('project_id')

                ]);

                return $this->response([], 200, 'Task Created Successfully');
            }
        } catch (\Exception $e) {
            return $this->response([
                'message' => $e->getCode() . ' ' . $e->getMessage(),
            ], $e->getCode(), );
        }
    }



    public function updateTask(Request $request, $taskGuid): JsonResponse
    {
        ProjectTask::where(['row_guid'=>$taskGuid])->update($request->all());
        return $this->response([], 201,'Task Updated');
    }

    
}
