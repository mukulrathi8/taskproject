<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = ['name','description','created_by','row_guid'];
    


    public function projectUsers():HasMany{
        return $this->hasMany(ProjectUser::class);       
    }

    public function projectTasks():HasMany{
        return $this->hasMany(ProjectTask::class);       
    }
}
