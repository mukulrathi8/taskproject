<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
    use HasFactory;

    
    public $timestamps = true;

    protected $fillable = ['name','description','row_guid','due_date','status','priority','project_id'];
}
