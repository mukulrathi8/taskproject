<?php

use App\Http\Controllers\api\{AuthController,ProjectController,ProjectTaskController};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('create-project', [ProjectController::class, 'createProject']);
    Route::get('list-project/{projectGuid?}', [ProjectController::class, 'listProjects']);
    Route::post('add-task', [ProjectTaskController::class, 'addTask']);
    Route::patch('update-task/{taskGuid}', [ProjectTaskController::class, 'updateTask']);
    Route::patch('update/{projectGuid}', [ProjectController::class, 'update']);
    Route::get('list-project-task/{projectGuid}', [ProjectController::class, 'listProjectTask']);
});

/* 
Auth Section  */
Route::post('create-account',[AuthController::class,'createAccount']);
Route::post('sign-in',[AuthController::class,'signIn'])->name('login');

Route::get('/', function (Request $request) {
    return json_encode(['data'=>'working']);
});

Route::post('',[AuthController::class,'signIn'])->name('login');


Route::fallback(function () {
    return response()->json([
        'status' => false,
        'message' => 'Page Not Found.',
    ], 404);
});